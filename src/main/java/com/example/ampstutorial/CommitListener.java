package com.example.ampstutorial;

import com.atlassian.event.Event;
import com.atlassian.event.EventListener;
import com.atlassian.fisheye.event.CommitEvent;
import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.services.RevisionDataService;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class CommitListener implements EventListener {
    private final RevisionDataService revisionDataService;
    private final TwitterLoginRecordStore twitterLoginRecordStore;

    public CommitListener(RevisionDataService revisionDataService, TwitterLoginRecordStore twitterLoginRecordStore) {
        this.revisionDataService = revisionDataService;
        this.twitterLoginRecordStore = twitterLoginRecordStore;
    }

    public void handleEvent(Event event) {
        if (event instanceof CommitEvent) {
            CommitEvent ce = (CommitEvent) event;
            ChangesetDataFE csData = revisionDataService.getChangeset(ce.getRepositoryName(), ce.getChangeSetId());
            if (csData != null) {
                TwitterLoginRecord twitterLogin = twitterLoginRecordStore.getByCommitterAndRepo(csData.getAuthor(), ce.getRepositoryName());
                if (twitterLogin != null) {
                    System.out.println("Found twitter login");
                    Twitter twitter = new Twitter(twitterLogin.getUserName(),twitterLogin.getPassword());
                    try {
                        twitter.updateStatus(csData.getComment());
                    } catch (TwitterException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    public Class[] getHandledEventClasses() {
        return new Class[] {CommitEvent.class};
    }
}
