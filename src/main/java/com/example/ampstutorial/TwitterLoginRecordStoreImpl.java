package com.example.ampstutorial;

import com.atlassian.crucible.spi.data.UserProfileData;
import com.atlassian.crucible.spi.services.ServerException;
import com.atlassian.crucible.spi.services.UserService;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TwitterLoginRecordStoreImpl implements TwitterLoginRecordStore {
    private final Map<CommitterAndRepoKey,TwitterLoginRecord> byCommitterAndRepository = new HashMap<CommitterAndRepoKey,TwitterLoginRecord>();
    private final Map<String,TwitterLoginRecord> byUser = new HashMap<String,TwitterLoginRecord>();
    private UserService userService;
    private PluginSettingsFactory pluginSettingsFactory;

    public TwitterLoginRecordStoreImpl() {
    }

    public void storeByUsername(String userName, TwitterLoginRecord record) {
        TwitterLoginRecord oldValue = byUser.put(userName, record);
        if (oldValue != null) {
            for (Map.Entry<CommitterAndRepoKey,TwitterLoginRecord> e : byCommitterAndRepository.entrySet()) {
                if (e.getValue() == oldValue) {
                    byCommitterAndRepository.remove(e.getKey());
                }
            }
        }
        try {
            UserProfileData userProfile = userService.getUserProfile(userName);
            for (Map.Entry<String, List<String>> repoCommitters : userProfile.getMappedCommitters().entrySet()) {
                for (String committer : repoCommitters.getValue()) {
                    byCommitterAndRepository.put(new CommitterAndRepoKey(committer, repoCommitters.getKey()), record);
                }
            }
        } catch (ServerException e) {
            throw new RuntimeException(e);
        }
    }

    public TwitterLoginRecord getByUsername(String userName) {
        return byUser.get(userName);
    }

    public TwitterLoginRecord getByCommitterAndRepo(String committer, String repoName) {
        return byCommitterAndRepository.get(new CommitterAndRepoKey(committer, repoName));
    }

    @Resource
    public void setPluginSettingsFactory(PluginSettingsFactory pluginSettingsFactory) {
        System.out.println("Injected " + pluginSettingsFactory);
        this.pluginSettingsFactory = pluginSettingsFactory;
    }
    @Resource
    public void setUserService(UserService userService) {
        System.out.println("Injected " + userService);

        this.userService = userService;
    }

    private static class CommitterAndRepoKey {
        private final String committer;
        private final String repoName;

        public CommitterAndRepoKey(String committer, String repoName) {
            this.committer = committer;
            this.repoName = repoName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CommitterAndRepoKey that = (CommitterAndRepoKey) o;

            if (committer != null ? !committer.equals(that.committer) : that.committer != null) return false;
            if (repoName != null ? !repoName.equals(that.repoName) : that.repoName != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = committer != null ? committer.hashCode() : 0;
            result = 31 * result + (repoName != null ? repoName.hashCode() : 0);
            return result;
        }
    }
}
