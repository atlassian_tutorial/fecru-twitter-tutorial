package com.example.ampstutorial;

import com.atlassian.crucible.spi.PluginId;
import com.atlassian.crucible.spi.PluginIdAware;
import com.atlassian.crucible.spi.data.UserData;
import com.atlassian.crucible.spi.services.ImpersonationService;
import com.atlassian.crucible.spi.services.ServerException;
import com.atlassian.crucible.spi.services.UserService;
import com.atlassian.fisheye.plugin.web.helpers.VelocityHelper;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ExampleServlet extends HttpServlet implements PluginIdAware {
    private final TemplateRenderer templateRenderer;
    private final ImpersonationService impersonationService;
    private final TwitterLoginRecordStore twitterLoginRecordStore;
    private PluginId pluginId;

    @Autowired
    public ExampleServlet(TemplateRenderer templateRenderer, ImpersonationService impersonationService,TwitterLoginRecordStore twitterLoginRecordStore) {
        this.templateRenderer = templateRenderer;
        this.impersonationService = impersonationService;
        this.twitterLoginRecordStore = twitterLoginRecordStore;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setAttribute("decorator", "fisheye.userprofile.tab");
        response.setContentType("text/html");
        Map<String,Object> params = new HashMap<String,Object>();
        TwitterLoginRecord loginRecord = getLoginRecord();
        if (loginRecord == null) {
            loginRecord = new TwitterLoginRecord("","");
        }
        params.put("loginRecord", loginRecord);
        templateRenderer.render("config.vm", params, response.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String newUserName = request.getParameter("userName");
        String newPassword = request.getParameter("password");
        if (newUserName != null && newPassword != null) {
            putLoginRecord(newUserName, newPassword);
        }
        response.sendRedirect("./example-servlet");
    }

    private void putLoginRecord(String newUserName, String newPassword) {
        twitterLoginRecordStore.storeByUsername(getCurrentUser().getUserName(), new TwitterLoginRecord(newUserName, newPassword));
    }

    public void setPluginId(PluginId pluginId) {
        this.pluginId = pluginId;
    }

    private UserData getCurrentUser() {
        try {
            return impersonationService.getCurrentUser(pluginId);
        } catch (ServerException e) {
            throw new RuntimeException(e);
        }
    }

    private TwitterLoginRecord getLoginRecord() {
        return twitterLoginRecordStore.getByUsername(getCurrentUser().getUserName());
    }
}