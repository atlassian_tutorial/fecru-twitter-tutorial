package com.example.ampstutorial;

public interface TwitterLoginRecordStore {
    void storeByUsername(String userName, TwitterLoginRecord record);
    TwitterLoginRecord getByUsername(String userName);
    TwitterLoginRecord getByCommitterAndRepo(String committer, String repoName);
}
